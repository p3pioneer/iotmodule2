# IIoT Protocols  

### A) 4-20 mA 
>  It is an ideal method of transferring process information because current does not change as it travels from transmitter to receiver.

##### Simplest 4-20 mA current loop Structure:
![](https://gitlab.com/p3pioneer/iotmodule2/-/raw/master/assignments/summary/PhotosinUse/4-20mA_current_loop_components.webp)  

* **`Sensors`**:
    * A sensor measures process variable like temp,humidity,flow,light,etc.  

* **`Transmitter`**:
    * Whatever a sensor senses ,we need a way to convert its measurement into equivalent 4-20 mA current signal,for these transmitter are used.  

* **`Power Source`**:
    * Every sensor/transmitter needs a power supply to produce a signal.
    > Be sure to consider that the power supply voltage must be at least 10% greater than the total voltage drop of the attached components (the transmitter, receiver and even wire).  

* **`Loop`**:
    * With the help of Power supply. Sensor senses the signal,transmitter converts into 4-20 mA equivalent current signal,all these is possible if there is a physical wire connection linked to each other forming a continuous Loop.

* **`Receiver`**:
    * The 4-20 mA current signal must be translated into units that can be easily understood by operators.So to intepret the current signal or display the information,Receiver are used in loop.  

| Pros | Cons |
| ------ | ------ |
| Dominant Standard in industries | Only transmit one particular process signal |
| Easy to connect & configure | when multiple loops are used,it becomes difficult to isolate these loops from ground loops |
| uses Less wires & connections|  |
| Better for long distance,as current don't degrade over distance like voltage| |
| Less sensitive to electrical noise| |
| since 4 mA is equal to 0% output,it is easy to detect faults| |  

[Why 0 mA is not possible?](https://www.predig.com/whitepaper/why-0-ma-signal-not-practical)  

### B) ModBus  
* It is a serial Communication Protocols ,for use with PLC's.It is a method used for transmitting information over serial lines between electronic devices.  
* The device requesting the information is called the Modbus Master and the devices supplying information are Modbus Slaves.  
* In a standard Modbus network, there is one Master and up to 247 Slaves, each with a unique Slave Address from 1 to 247. The Master can also write information to the Slaves.  
* Communication between a master and a slave occurs in a frame that indicates a function code.  
* The function code identifies the action to perform, such as read a discrete input; read a first-in, first-out queue; or perform a diagnostic function.  
* The slave then responds, based on the function code received.  

![](assignments/summary/PhotosinUse/Modbus-Communication-Protocol-Structure.png)  
[Detailed study of bytes of modbus message](https://www.se.com/us/en/faqs/FA168406/)  

* ModBus Protocol can be used over 2 interfaces:  
    1. **`RS485(ModBus RTU)`**:
        * It is a _serial transmission_ standard (i.e like UART),You can put several RS485 devices on same bus.  
        * RS485 is not directly compatible: you must use the correct type of interface, or the signals won't go through. Mainly done through an easy to use an RS485 to USB.  
    2. **`Ethernet(ModBus TCP/IP)`**:
        * Modbus TCP/IP uses a physical network (Ethernet), with a networking standard (TCP/IP), and itself offers a method of representing data (Modbus as the application protocol).  
        *  TCP/IP is simply a transport layer protocol and does not change the way data is stored or interpreted inside the message.  

### C) OPCUA (Open Platform Communications United Architecture)   
* It is a _data exchange_ standard for industrial communication (machine-to-machine or PC-to-machine communication).  
* This open interface standard is independent of the manufacturer or system supplier of the application, of the programming language in which the respective software was programmed, and of the operating system on which the application is running.  

| OPC Server | OPC Client |
| ------ | ------ |
| ![](assignments/summary/PhotosinUse/OPC-Server.png) | ![](assignments/summary/PhotosinUse/OPC-Client.png) |
|  It is a software that implements the OPC standard and thus provides the standardized OPC interfaces to the outside world. | The OPC Client is the logical counterpart to the OPC Server. The OPC Server can be connected to the OPC Client and read out the data provided by the Server. |
| The OPC Server from the manufacturer can be supplied as stand-alone software or as an embedded OPC Server on the device or the machine controller. | What happens to the data in the client is very application-specific.Common application are scada or mes systems. |  

#### OPC Architecture
![](assignments/summary/PhotosinUse/opc_arch.gif) 

![](assignments/summary/PhotosinUse/opcua_iot_broker.jpg)

[To know specs,security,types of OPC,Click here](https://www.opc-router.com/what-is-opc-ua/)  


# Cloud Protocols  

> The cloud computing protocols are a set of rules that allow 2 electronic elements to connect and exchange information with each other. It is used for storage, communication, encryption, decryption, networks, security, user login management, etc.  

### MQTT (Message Queuing Telemetry Transport)  
* It is a lightweight publish/subscribe messaging protocol designed for M2M (machine to machine) telemetry in low bandwidth environments.  
* A perfect solution for IOT applications, also allows to send commands to control outputs, read and publish data from sensor nodes.  
* Communication between several devices can be established simultaneously.  

##### Understanding MQtt protocol
* It surrounds around 2 things: client and broker.  
* An MQTT broker is a server while client is connected devices.  
* When a client wants to send data to server it is called **Publish**.  
* When a server wants to send data to client it is called **Subscribe**.  

**Working model of publish/subscribe cycle**  

![](assignments/summary/PhotosinUse/MQTT_publish-subscribe.jpg)   
 
### HTTP (Hyper Text Transfer Protocol)  
* It is used to transfer data across Web.  
* It is a command and respond text based protocol using a client/server model.  
* The client makes a request and the server responds.  

![](assignments/summary/PhotosinUse/HTTP-Protocol-Basics.jpg)     

### Request and Response Structure:  
1. `Request`:  
![](assignments/summary/PhotosinUse/HTTP_Request.png)  

Different types of method for request are:  

| Method | Description |
| ------ | ------ |
| GET | Used to retrieve information from the given URL(ex: visiting a page) |
| POST | Used to send data to the server(ex: submitting a form ) |
| DELETE | Delete a File of the specified URL |
| PUT | Uploads a File of the specified URL  |  

2. `Response`:  
![](assignments/summary/PhotosinUse/HTTP_Response.png)  

The server will send status code ,depending upon the response,here are some common faced status code and what it means:  
![](assignments/summary/PhotosinUse/statuscode.png)









 
        



