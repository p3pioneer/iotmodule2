# Electrical Safety Instructions  
> All electrical systems have the potential to cause harm. Your body is a natural conductor of electricity, and is vulnerable to electrical shocks and burns—including thermal burns that affect both the external skin and internal tissues—and arc blasts that can cause your lungs to collapse, or muscles to contract, causing a serious fall.So one must take necessary precaution while working with electrical systems.

1. **`Power Supply`**
    * Do's
        - Always make sure that the output voltage of the power supply matches the input voltage of the board.
        - While turning on the power supply make sure every circuit is connected properly.
        - If you smell anything burning, immediately disconnect the power and examine your circuit to find out what went wrong.  
    * Don'ts
        - Do not connect power supply without matching the power rating.
        - Never connect a higher output(12V/3Amp) to a lower (5V/2Amp) input.
        - Do not try to force the connector into the power socket ,it may damage the connector.  

![](assignments/summary/PhotosinUse/Electrical_Safety_Tips.png)   

2. **`Handling`**
    * Do's
        - Treat every device like it is energized, even if it does not look like it is plugged in or operational.
        - While working keep the board on a flat stable surface (wooden table).
        - Unplug the device before performing any operation on them.
        - When handling electrical equipment, make sure your hands are dry.
        - Keep all electrical circuit contact points enclosed.
        - If the board becomes too hot try to cool it with a external usb fan.
    * Don'ts 
        - Don’t handle the board when its powered ON.
        - Never touch electrical equipment when any part of your body is wet, (that includes fair amounts of perspiration).
        - Do not touch any sort of metal to the development board.  



3. **`General Purpose I/O`**
> It’s a standard interface used to connect microcontrollers to other electronic devices.  
![](assignments/summary/PhotosinUse/gpio.png)  


* Do's 
    - Find out whether the board runs on 3.3v or 5v logic.
    - Always connect the LED (or sensors) using appropriate resistors.
    - To Use 5V peripherals with 3.3V we require a logic level converter.
* Don'ts
    - Never connect anything greater that 5v to a 3.3v pin.
    - Avoid making connections when the board is running.
    - Don't plug anything with a high (or negative) voltage.
    - Do not connect a motor directly , use a transistor to drive it.

### Guidelines for using UART,SPI,and I2C Interface

![](assignments/summary/PhotosinUse/uart-spi-i2c.png)  

[Click Here for detailed comparison of UART,SPI and I2C Interface](https://www.seeedstudio.com/blog/2019/09/25/uart-vs-i2c-vs-spi-communication-protocols-and-uses/)  

1. **`UART`**:Simple serial communication protocol that allows the host communicate with the auxiliary device.  
    
    - Connect Rx pin of device1 to Tx pin of device2 ,similarly Tx pin of device1 to Rx pin of device2.
    - If the device1 works on 5v and device2 works at 3.3v then use the level shifting mechanism(voltage divider ).
    - Genrally UART is used to communicate with board through USB to TTL connection . USB to TTL connection does not require a protection circuit.
    - Whereas Senor interfacing using UART might require a protection circuit.

2. **`I2C`**:It is a serial communications protocol similarly to UART but not used for PC-device communication but are used with modules and sensors.  
    - while using I2c interfaces with sensors SDA and SDL lines must be protected.
    - Protection of these lines is done by using pullup registers on both lines.
    - If you use the inbuilt pullup registers in the board you wont need an external circuit.
    - If you are using bread-board to connect your sensor , use the pullup resistor.        
    - Generally , 2.2kohm <= 4K ohm resistors are used.

3. **`SPI`**:It is similar to I2C and it is a different form of serial-communications protocol specially designed for microcontrollers to connect. 
    - Generally ,Spi in development boards is in Push-pull mode.
    - Push-pull mode does not require any protection circuit.
    - on Spi interface if you are using more than one slaves it is possible that the device2 can "hear" and "respond" to the master's communication with device1- which is an disturbance.
    - To overcome this problem , we use a protection circuit with pullup resistors on each the Slave Select line(CS).
    - Resistors value can be between 1kOhm ~10kOhm .Generally 4.7kOhm resistor is used.